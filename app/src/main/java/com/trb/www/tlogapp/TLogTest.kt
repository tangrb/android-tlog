package com.trb.www.tlogapp

import com.trb.www.tlog.TLog
import java.util.concurrent.TimeUnit

/**
 * author   唐荣兵
 * datetime 4/12/21 11:17 AM
 * email    yanhuang8923@163.com
 * mobile   186_7796_8417
 * <p>
 * Function description：
 */
open class TLogTest {
    fun test() {
        TLog.kv("TLogTest with Kotlin")
        TLog.kd("TLogTest with Kotlin")
        TLog.ki("TLogTest with Kotlin")
        TLog.kw("TLogTest with Kotlin")
        TLog.ke("TLogTest with Kotlin")
    }

    fun testLoopTask() {
        Thread {
            var count = 0
            while (count < 1000) {
                count += 1
                TimeUnit.MILLISECONDS.sleep(10)
                TLog.kv(count)
                TLog.kd(count)
                TLog.ki(count)
                TLog.kw(count)
                TLog.ke(count)
            }
        }.start()
    }
}