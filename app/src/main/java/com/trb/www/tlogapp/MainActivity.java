package com.trb.www.tlogapp;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.trb.www.tlog.TLog;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TLog.startCatchLog((level, log) -> Log.d(TAG, "onCreate: level=" + level + ", log=" + log));

        TLog.v("log v test");
        TLog.d("log d test");
        TLog.i("log i test");
//        TLog.w("log w test");
        TLog.e("log e test");
//        TLog.w(new Exception("level w exception"));
        TLog.w("log w test", new Exception("level w exception"));


        TLogTest tLogTest = new TLogTest();
        tLogTest.test();

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TLog.d("called");
            }
        });

        findViewById(R.id.button2).setOnClickListener(v -> TLog.d("called"));

        findViewById(R.id.button3).setOnClickListener(v -> tLogTest.testLoopTask());

        findViewById(R.id.button4).setOnClickListener(v -> testLogCatch());
    }

    private void testLogCatch() {
        System.out.println("called");

        TLog.startCatchLog((level, msg) -> System.out.println("level -- " + level + " >>> msg -- " + msg));

        new Thread(() -> {
            int count = 0;
            while (count < 100) {
                count ++;

                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                TLog.v(true, count);
                TLog.d(true, count);
                TLog.i(true, count);
                TLog.w(true, count);
                TLog.e(true, count);
            }

            try {
                TimeUnit.MILLISECONDS.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            TLog.stopCatchLog();
        }).start();
    }

}