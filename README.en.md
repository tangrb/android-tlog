# Android-TLog

#### 介绍
Android平台用来简化日志打印和日志过滤的库。

1. Support log leve: Verbose、Debug、Info、Warning、Error;
2. Support code location，Java and Kotlin;
3. Supports handling and storing exceptions not caught by the current thread;
4. Supports direct printing of basic data types (as all basic data types will be wrapped as objects).


#### Notice
V2023.06.15.01-release is not compatible with v2021.04.19.03-release, but download of v2021.04.19.03-release is still provided.


#### Use manual

1. Download this project，and then pack it as .jar or .aar

2. add gradle dependency

```gradle
allprojects {
    repositories {
		...
        maven { url 'https://jitpack.io' }
    }
}
```

```gradle
dependencies {
    implementation 'com.gitee.tangrb:android-tlog:v2023.06.15.01-release'
}
```


The use demo is in MainActivity.

