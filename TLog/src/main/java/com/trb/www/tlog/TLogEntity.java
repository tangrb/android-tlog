package com.trb.www.tlog;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * author   唐荣兵
 * datetime 4/12/21 2:16 PM
 * email    yanhuang8923@163.com
 * mobile   186_7796_8417
 * <p>
 * Function description：
 */
final class TLogEntity {
    //年-月-日 时:分:秒.毫秒/包名 LEVEL/TAG: log
    private static final String FMT_LOG = "%04d-%02d-%02d %02d:%02d:%02d.%03d/%s %s/%s: %s";

    public String pkgName;
    public TLogLevel level;
    public String tag;
    public String msg;
    public Throwable tr;
    private final Calendar calendar;

    public TLogEntity(String pkgName, TLogLevel level, String tag, String msg, Throwable tr) {
        calendar = GregorianCalendar.getInstance();
        this.pkgName = pkgName;
        this.level = level;
        this.tag = tag;
        this.msg = msg;
        this.tr = tr;
    }

    public String buildLog() {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int millisecond = calendar.get(Calendar.MILLISECOND);

        return String.format(Locale.getDefault(),
                FMT_LOG,
                year, month, day, hour, minute, second, millisecond,
                pkgName, getLevelText(),
                tag, getLog());
    }

    @NonNull
    private String getLevelText() {
        if (level == TLogLevel.v) {
            return "V";
        } else if (level == TLogLevel.d) {
            return "D";
        } else if (level == TLogLevel.i) {
            return "I";
        } else if (level == TLogLevel.w) {
            return "W";
        } else {
            return "E";
        }
    }

    @NonNull
    private String getLog() {
        if (msg == null && tr == null) {
            return "";
        }

        return (msg == null ? "" : msg) + "\n" + Log.getStackTraceString(tr);
    }
}
