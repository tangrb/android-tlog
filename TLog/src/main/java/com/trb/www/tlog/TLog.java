package com.trb.www.tlog;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * author   唐荣兵
 * datetime 4/12/21 10:29 AM
 * email    yanhuang8923@163.com
 * mobile   186_7796_8417
 * <p>
 * Function description：
 */
public final class TLog {
    //日志打印的等级：默认全部开启
    public static boolean printV = true;
    public static boolean printD = true;
    public static boolean printI = true;
    public static boolean printW = true;
    public static boolean printE = true;


    //************************************************************************************************************
    private static final String FMT_PRINT_TAG_J = "%s(%s.java:%d)";
    private static final String FMT_PRINT_TAG_K = "%s(%s.kt:%d)";
    private static final String FMT_PRINT_TAG_K_NEW = "%s(%s:%d)";

    private static String buildTagTextJava(TLogTag logTag) {
        return String.format(Locale.getDefault(),
                FMT_PRINT_TAG_J,
                logTag.methodName,
                logTag.simpleClassName,
                logTag.line);
    }

    private static String buildTagTextKotlin(TLogTag logTag) {
        return String.format(Locale.getDefault(),
                logTag.simpleClassName.endsWith(".kt") ? FMT_PRINT_TAG_K_NEW : FMT_PRINT_TAG_K,
                logTag.methodName,
                logTag.simpleClassName,
                logTag.line);
    }
    //************************************************************************************************************


    //************************************************************************************************************
    private static Thread logCatchThread;
    private static volatile boolean exitCatch = false;
    private static final Lock logCatchThreadLock = new ReentrantLock();
    private static final ConcurrentLinkedQueue<TLogEntity> logEntities = new ConcurrentLinkedQueue<>();


    /**
     * 开始抓取日志
     *
     * @param listener {@link OnTLogCatchListener}
     */
    public static void startCatchLog(@NonNull OnTLogCatchListener listener) {
        synchronized (TLog.class) {
            if (logCatchThread != null) return;

            logCatchThread = new Thread(() -> {
                exitCatch = false;
                try {
                    while (!exitCatch) {
                        synchronized (logCatchThreadLock) {
                            if (logEntities.isEmpty()) {
                                logCatchThreadLock.wait();
                            }

                            if (exitCatch) break;
                        }

                        TLogEntity entity = logEntities.poll();
                        if (entity == null) continue;

                        listener.onTLogCatch(entity.level, entity.buildLog());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    logEntities.clear();
                }
            });

            logCatchThread.start();
        }
    }

    /**
     * 停止抓取日志
     */
    public static void stopCatchLog() {
        synchronized (TLog.class) {
            if (logCatchThread != null) {
                exitCatch = true;
                synchronized (logCatchThreadLock) {
                    logCatchThreadLock.notifyAll();
                }
                logEntities.clear();
                logCatchThread = null;
            }
        }
    }

    private static void catchLog(@NonNull TLogLevel level, @NonNull String pkgName, @NonNull String tagText, @Nullable String msg, @Nullable Throwable tr) {
        synchronized (TLog.class) {
            if (exitCatch) return;

            logEntities.add(new TLogEntity(pkgName, level, tagText, msg, tr));
            synchronized (logCatchThreadLock) {
                logCatchThreadLock.notifyAll();
            }
        }
    }
    //************************************************************************************************************


    //************************************************************************************************************

    /**
     * 处理未捕获的异常。
     * <p>
     * 未捕获的异常会存储在目录 {@link Context#getFilesDir()} 下的子目录 "/TLog/crash"中，
     * 每一个未捕获的异常都会单独存储在一个文件中，
     * 该文件的名称类似 "crash_t_log_2021-04-14 12:12:12.120.txt"。
     * <p>
     * 该方法一般在Application初始化的时候进行调用。
     *
     * @param context Application的Context
     */
    public static void handleUncaughtException(Context context) {
        Thread.currentThread().setUncaughtExceptionHandler((t, e) -> {
            File crashDir = new File(context.getFilesDir().getAbsolutePath() + "/TLog/crash");

            if (!crashDir.exists()) {
                if (!crashDir.mkdirs()) {
                    System.err.println("current application don't file system option permission!");
                    return;
                }
            }

            Calendar calendar = GregorianCalendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);
            int millisecond = calendar.get(Calendar.MILLISECOND);

            String crashFileName = String.format(Locale.getDefault(),
                    "crash_t_log_%04d-%02d-%02d %02d:%02d:%02d.%03d.txt",
                    year, month, day, hour, minute, second, millisecond);
            String crashFileAbsPath = crashDir.getAbsolutePath() + "/" + crashFileName;
            File crashFile = new File(crashFileAbsPath);

            try (OutputStream outs = new FileOutputStream(crashFile);
                 PrintStream ps = new PrintStream(outs, true)) {
                e.printStackTrace(ps);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
    }
    //************************************************************************************************************


    //************************************************************************************************************
    public static void v(@NonNull Object msg) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.v(tagText, msg.toString());
        }
    }

    public static void v(boolean isSave, @NonNull Object msg) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.v(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.v, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void kv(@NonNull Object msg) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.v(tagText, msg.toString());
        }
    }

    public static void kv(boolean isSave, @NonNull Object msg) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.v(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.v, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void v(@NonNull Object msg, @NonNull Throwable tr) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.v(tagText, msg.toString(), tr);
        }
    }

    public static void v(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.v(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.v, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }

    public static void kv(@NonNull Object msg, @NonNull Throwable tr) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.v(tagText, msg.toString(), tr);
        }
    }

    public static void kv(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printV) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.v(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.v, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }
    //************************************************************************************************************


    //************************************************************************************************************
    public static void d(@NonNull Object msg) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.d(tagText, msg.toString());
        }
    }

    public static void d(boolean isSave, @NonNull Object msg) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.d(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.d, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void kd(@NonNull Object msg) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.d(tagText, msg.toString());
        }
    }

    public static void kd(boolean isSave, @NonNull Object msg) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.d(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.d, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void d(@NonNull Object msg, @NonNull Throwable tr) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.d(tagText, msg.toString(), tr);
        }
    }

    public static void d(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.d(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.d, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }

    public static void kd(@NonNull Object msg, @NonNull Throwable tr) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.d(tagText, msg.toString(), tr);
        }
    }

    public static void kd(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printD) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.d(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.d, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }
    //************************************************************************************************************


    //************************************************************************************************************
    public static void i(@NonNull Object msg) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.i(tagText, msg.toString());
        }
    }

    public static void i(boolean isSave, @NonNull Object msg) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.i(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.i, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void ki(@NonNull Object msg) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.i(tagText, msg.toString());
        }
    }

    public static void ki(boolean isSave, @NonNull Object msg) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.i(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.i, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void i(@NonNull Object msg, @NonNull Throwable tr) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.i(tagText, msg.toString(), tr);
        }
    }

    public static void i(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.i(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.i, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }

    public static void ki(@NonNull Object msg, @NonNull Throwable tr) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.i(tagText, msg.toString(), tr);
        }
    }

    public static void ki(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printI) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.i(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.i, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }
    //************************************************************************************************************


    //************************************************************************************************************
    public static void w(@NonNull Object msg) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.w(tagText, msg.toString());
        }
    }

    public static void w(boolean isSave, @NonNull Object msg) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.w(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.w, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void kw(@NonNull Object msg) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.w(tagText, msg.toString());
        }
    }

    public static void kw(boolean isSave, @NonNull Object msg) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.w(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.w, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void w(@NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.w(tagText, tr);
        }
    }

    public static void w(boolean isSave, @NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.w(tagText, tr);

            if (isSave) {
                catchLog(TLogLevel.w, tag.pkgName, tagText, null, tr);
            }
        }
    }

    public static void kw(@NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.w(tagText, tr);
        }
    }

    public static void kw(boolean isSave, @NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.w(tagText, tr);

            if (isSave) {
                catchLog(TLogLevel.w, tag.pkgName, tagText, null, tr);
            }
        }
    }

    public static void w(@NonNull Object msg, @NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.w(tagText, msg.toString(), tr);
        }
    }

    public static void w(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.w(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.w, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }

    public static void kw(@NonNull Object msg, @NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.w(tagText, msg.toString(), tr);
        }
    }

    public static void kw(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printW) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.w(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.w, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }
    //************************************************************************************************************


    //************************************************************************************************************
    public static void e(@NonNull Object msg) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.e(tagText, msg.toString());
        }
    }

    public static void e(boolean isSave, @NonNull Object msg) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.e(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.e, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void ke(@NonNull Object msg) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.e(tagText, msg.toString());
        }
    }

    public static void ke(boolean isSave, @NonNull Object msg) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.e(tagText, msgStr);

            if (isSave) {
                catchLog(TLogLevel.e, tag.pkgName, tagText, msgStr, null);
            }
        }
    }

    public static void e(@NonNull Object msg, @NonNull Throwable tr) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);
            Log.e(tagText, msg.toString(), tr);
        }
    }

    public static void e(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextJava(tag);

            String msgStr = msg.toString();
            Log.e(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.e, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }

    public static void ke(@NonNull Object msg, @NonNull Throwable tr) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);
            Log.e(tagText, msg.toString(), tr);
        }
    }

    public static void ke(boolean isSave, @NonNull Object msg, @NonNull Throwable tr) {
        if (printE) {
            TLogTag tag = new TLogTag();
            String tagText = buildTagTextKotlin(tag);

            String msgStr = msg.toString();
            Log.e(tagText, msgStr, tr);

            if (isSave) {
                catchLog(TLogLevel.e, tag.pkgName, tagText, msgStr, tr);
            }
        }
    }
    //************************************************************************************************************
}
