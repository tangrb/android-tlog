package com.trb.www.tlog;

/**
 * author   唐荣兵
 * datetime 4/12/21 10:49 AM
 * email    yanhuang8923@163.com
 * mobile   186_7796_8417
 * <p>
 * Function description：
 */
public enum TLogLevel {
    v, d, i, w, e
}
