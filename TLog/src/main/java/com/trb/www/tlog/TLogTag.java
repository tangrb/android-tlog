package com.trb.www.tlog;

/**
 * author   唐荣兵
 * datetime 4/12/21 10:29 AM
 * email    yanhuang8923@163.com
 * mobile   186_7796_8417
 * <p>
 * Function description：
 */
final class TLogTag {
    String className;
    String pkgName;
    String simpleClassName;
    String methodName;
    int line;

    public TLogTag() {
        StackTraceElement caller = new Throwable().getStackTrace()[2];
        className = caller.getClassName();
        int index = className.lastIndexOf(".");
        pkgName = className.substring(0, index);
        simpleClassName = className.substring(index + 1);
        methodName = caller.getMethodName();
        if (simpleClassName.contains("$")) {
            index = simpleClassName.indexOf("$");
            methodName = simpleClassName.substring(index) + "." + methodName;
            simpleClassName = simpleClassName.substring(0, index);
        }
        line = caller.getLineNumber();
    }
}
