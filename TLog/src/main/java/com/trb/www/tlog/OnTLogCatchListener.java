package com.trb.www.tlog;

import androidx.annotation.NonNull;

/**
 * author   唐荣兵
 * datetime 4/12/21 2:07 PM
 * email    yanhuang8923@163.com
 * mobile   186_7796_8417
 * <p>
 * Function description：
 */
public interface OnTLogCatchListener {
    void onTLogCatch(@NonNull TLogLevel level, @NonNull String msg);
}
