# Android-TLog

#### 介绍
Android平台用来简化日志打印和日志过滤的库。

1. 支持Verbose、Debug、Info、Warning、Error这5个级别的日志打印；
2. 支持Java代码和Kotlin代码的定位；
3. 支持处理并存储当前线程未捕获的异常；
4. 支持直接打印基本数据类型（因为所有的基本数据类型都将被包装为对象）。


#### 注意
v2023.06.15.01-release 与 v2021.04.19.03-release 不相互兼容，但是仍然提供了v2021.04.19.03-release的下载。

#### 使用说明

1. 自己下载该工程，然后打包成jar或者aar文件进行使用。

2. 添加gradle依赖

```gradle
allprojects {
    repositories {
		...
        maven { url 'https://jitpack.io' }
    }
}
```

```gradle
dependencies {
    implementation 'com.gitee.tangrb:android-tlog:v2023.06.15.01-release'
}
```


使用示例在MainActivity中。
